class Dictionary(dictionary: List<String>) : ArrayList<String>(dictionary) {

    private val startsWithComparator: java.util.Comparator<String>
        get() {
            return Comparator { o1, o2 ->
                if (o1.startsWith(o2))
                    return@Comparator 0
                o1.compareTo(o2)
            }
        }

    public fun filter(string: String) {
        val fromIndex = findFirstOfFiltered(string)
        if (fromIndex == size){
            this.clear()
            return
        }

        this.removeRange(0, fromIndex)
        val toIndex = findLastOfFiltered(string)
        removeRange(toIndex + 1, size)
    }

    private fun findFirstOfFiltered(string: String): Int {
        var left = size;
        var finalLeft = left
        while (left >= 0) {
            finalLeft = left
            left = this.binarySearch(string, startsWithComparator, 0, left)
        }
        return finalLeft
    }

    private fun findLastOfFiltered(string: String): Int {
        var right = 0
        var finalRight = right
        while (right >= 0 && right + 1 < size) {
            finalRight = right
            right = binarySearch(string, startsWithComparator, right + 1, size)
        }
        return finalRight
    }
}
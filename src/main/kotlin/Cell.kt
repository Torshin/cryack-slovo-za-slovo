class Cell {
    val letter: String
    val x: Int
    val y: Int

    constructor(letter: String, x: Int, y: Int) {
        this.letter = letter
        this.x = x
        this.y = y
    }

    var inUse: Boolean = false
}
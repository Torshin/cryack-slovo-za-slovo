import java.util.*

class Word : ArrayDeque<Cell>(), Comparable<String> {
    override fun addLast(e: Cell) {
        super.addLast(e)
        e.inUse = true
    }

    override fun removeLast(): Cell {
        val pop = super.removeLast()
        pop.inUse = false
        return pop
    }

    override fun toString(): String {
        return this.joinToString("") { it.letter }
    }

    override fun compareTo(other: String): Int {
        if (size > 0 && other.isNotEmpty()) {
            var compareTo: Int = 0
            val tempWord = Word()
            for (i in (0..size)) {
                val first = removeFirst()
                compareTo = first.letter[0].compareTo(other[i])
                tempWord.addLast(first)
            }
            tempWord.forEach{ addFirst(it)}
            return compareTo

        }
        return 0
    }

}

class Field(string: String) : ArrayList<ArrayList<Cell>>(DEFAULT_SIZE) {
    init {
        for (i in (0 until DEFAULT_SIZE)) {
            this.add(ArrayList(DEFAULT_SIZE))
            for (j in (0 until DEFAULT_SIZE)) {
                val currentIndex = i * DEFAULT_SIZE + j
                val currentLetter = string[currentIndex].toString().toLowerCase()
                this[i].add(Cell(currentLetter, j, i))
            }
        }
    }


    companion object {
        const val DEFAULT_SIZE = 5
    }

    fun clearField() {
        forEach { it.forEach { it.inUse = false } }
    }

    fun getAvailableCells(cell: Cell): ArrayList<Cell> {
        val result = arrayListOf<Cell>()
        for (y in (cell.y - 1..cell.y + 1)) {
            for (x in (cell.x - 1..cell.x + 1)) {
                if (x >= 0 && y >= 0 && x < DEFAULT_SIZE && y < DEFAULT_SIZE) {
                    val currentCell = this[y][x]
                    if (!currentCell.inUse) {
                        result.add(currentCell)
                    }
                }
            }
        }
        return result
    }

    fun getAvailableCells(word: Word): ArrayList<Cell> {
        val result = arrayListOf<Cell>()
        val cell = word.last
        for (y in (cell.y - 1..cell.y + 1)) {
            for (x in (cell.x - 1..cell.x + 1)) {
                if (x >= 0 && y >= 0 && x < DEFAULT_SIZE && y < DEFAULT_SIZE) {
                    val currentCell = this[y][x]
                    if (!word.contains(currentCell)) {
                        result.add(currentCell)
                    }
                }
            }
        }
        return result
    }
}
import java.io.File

class WordsFinder(private val field: Field) {
    private val words = arrayListOf<Word>()
    private val dictionary = File("zdf-win.txt").bufferedReader().readLines()
//    private var filtered: Dictionary = Dictionary(dictionary)

    fun find(): ArrayList<Word> {
        field.forEach {
            it.forEach {
                val word = Word()
                word.addLast(it)
                findWords(word)
            }
        }
        return words
    }

    private fun findWords(word: Word) {
        val availableCells = field.getAvailableCells(word)
        var filtered = Dictionary(dictionary)
        availableCells.forEach {
            word.addLast(it)
            filtered.filter(word.toString())

            if (filtered.isNotEmpty()) {
                if (filtered.any { it.length == word.size }) {
                    words.add((word.clone() as Word))
                }
                findWords(word)
            }
            word.removeLast()
            filtered = Dictionary(dictionary)
        }
    }
}





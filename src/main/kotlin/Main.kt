//  х и м и я  ф и з и к  М а ш у н  Д и м у л  Б о б у л
fun main() {
    println("Enter field:")
    val string: String = readLine()!!
    val trimmed = string.trim().replace(" ", "")
    val field = Field(trimmed)

    val words = WordsFinder(field).find()

    words.sortByDescending { it.size - 0.1 * it.first.y - 0.01 * it.first.x }
    var length = words.first().size
    words.map { it.toString() }.toSet().forEach {
        if (it.length != length) {
            println()
            length = it.length
        }
        print(it + "\t")
    }
    println()
}
